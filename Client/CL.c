/***********************************************/
/* Solution Client (-serveur) version 2023     */
/* Nov. 2023                                   */
/* gcc -std=c11 -Wall CL.c -o CL               */
/* Pour exécuter : ./CL			       */
/***********************************************/

#include "CL_def.h"
#include "SV_sem.h"
#include "SV_mem.h"

void handler_SIGUSR1();
void handler_SIGUSR2();
void handler_SIGTERM();

void* thread_lecteur(void* data);
pthread_t threads[2];
pthread_mutex_t pthread_mutex = PTHREAD_MUTEX_INITIALIZER;
char dataReady = 0;
char terminaison = 0;

int main(int argc, char *argv[]){
	BUF *Tptr;
	int shmid;
	int mutex;

	/* SETUP SIGNAL */
	printf("\n####  PID du client : %d  ####\n\n", getpid());

/*/////////////////////// Pour la reception des signaux //////////////////////*/
	
	signal(SIGUSR1, handler_SIGUSR1);
	signal(SIGUSR2, handler_SIGUSR2);
	signal(SIGTERM, handler_SIGTERM);

/*/////////////////////////// En attente du lancement du serveur  //////////////////////*/
	
	pause(); 
	pause(); //IMPORTANT : ATTEND LA CREATION DU MUTEX ET DE LA MEMOIRE PARTAGEE PAR LE SERVEUR
	
	/* INIT MUTEX */
	if((mutex = CreationMutex()) == -1){
		perror("Erreur creation mutex.\n");
		exit(0);
	}

	/* INIT MEMOIRE */
	shmid = AllocTampon(&Tptr);
	printf("ID memoire partagee : %d\n", shmid);

	/* INIT THREAD LECTEUR */
	pthread_create(&(threads[0]), NULL, thread_lecteur, Tptr);
	

/*//////////////////////////// Boucle principale  /////////////////////////////*/
   
	while (!terminaison){
		//printf("MAIN : BUF[%d]=%d\n", Tptr->n, Tptr->tampon[Tptr->n]);
		pthread_mutex_lock(&pthread_mutex);
		dataReady = 1;
		pthread_mutex_unlock(&pthread_mutex);
		
		pause();    /* On attend un signal */
		
	}

	/*////////////////////////////fin du programme  /////////////////////////////*/
	
	pthread_join(threads[0], NULL);
	pthread_mutex_destroy(&pthread_mutex);
	return 0;
}	   	   

	   	   	   
/*/////////////////////////////////////////////////////////////////////////*/
	   
void handler_SIGUSR1() /* reception SIGUSR 1 */
{
	printf("Nouvelle donnee!\n");
	signal(SIGUSR1,handler_SIGUSR1);
}

void handler_SIGUSR2(){
    printf("Requete de fin\n");
    terminaison = 1;
}

void handler_SIGTERM(){
	printf("Fin forcee");	
}

void* thread_lecteur(void *data){
	BUF* mem = (BUF*) data;
	while(!terminaison){
		if(dataReady){
			pthread_mutex_lock(&pthread_mutex);
			printf("BUF[%d]=%d\n", mem->n, mem->tampon[mem->n]);
			dataReady = 0;
			pthread_mutex_unlock(&pthread_mutex);
		}
		sched_yield();
	}
	pthread_exit(NULL);
}